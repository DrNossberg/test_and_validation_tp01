using Xunit;
using FizzBuzz;

public class UnitTest
{
    // Arrange
    FizzBuzzGenerator generator = new FizzBuzzGenerator();


    [Theory]
    [InlineData(1)]
    [InlineData(-1)]
    [InlineData(14)]
    [InlineData(150)]
    public void WrongArgsInt(int number)
    {
        // Act
        string result = generator.Generer(number);

        // Assert
        Assert.Empty(result);
    }

    [Theory]
    [InlineData(16, "12Fizz4BarrFizz78FizzBarr11Fizz1314FizzBarr16")]
    [InlineData(149, "12Fizz4BarrFizz78FizzBarr11Fizz1314FizzBarr1617Fizz19BarrFizz2223FizzBarr26Fizz2829FizzBarr3132Fizz34BarrFizz3738FizzBarr41Fizz4344FizzBarr4647Fizz49BarrFizz5253FizzBarr56Fizz5859FizzBarr6162Fizz64BarrFizz6768FizzBarr71Fizz7374FizzBarr7677Fizz79BarrFizz8283FizzBarr86Fizz8889FizzBarr9192Fizz94BarrFizz9798FizzBarr101Fizz103104FizzBarr106107Fizz109BarrFizz112113FizzBarr116Fizz118119FizzBarr121122Fizz124BarrFizz127128FizzBarr131Fizz133134FizzBarr136137Fizz139BarrFizz142143FizzBarr146Fizz148149")]
    public void RightTest(int number, string expectedString) {
        // Act
        string result = generator.Generer(number);

        // Assert
        Assert.Equal(expectedString, result);
    }
}