﻿using System;

namespace Les_impots;

public class Calculateur
{
    private const string _messageInvalideParameter = "Invalid parameters";
    private int _taxeRate = 0;

    static void Main(string[] args)
    {
        Calculateur calc = new Calculateur();
        int income = int.Parse(args[0]);

        Console.WriteLine($"Taux d'imposition {calc.getTaxesRates( income )}%");
        Console.WriteLine($"Montant d'imposition {calc.getTaxesValue( income, calc._taxeRate )} €");
    }

    public float getTaxesValue(int income, int rate)
    {
        if (income <= 0)
            throw new ArgumentException(_messageInvalideParameter);
        return ((float)income * rate / 100);
    }

    public int getTaxesRates(int income)
    {
        int[,] rates = {{ 10778, 0 }, { 27479, 11 }, { 78571, 30 }, { 168995, 41 } };

        if (income <= 0)
            throw new ArgumentException(_messageInvalideParameter);
        for (int i = 0; i < 4; i++)
            if (income < rates[i, 0]) {
                _taxeRate = rates[i, 1];
                return _taxeRate;
            }
        _taxeRate = 45;
        return (_taxeRate);
    }
}