using Xunit;
using JeuDeTennis;

namespace JeuDeTennisTest;

public class UnitTest
{
    Jeu jeu = new Jeu();

    [Theory]
    [InlineData(1, "0")]
    [InlineData(0, "0")]
    public void GetStartPoints(int joueur_id, string expectedPoints)
    {
        // Act
        string result = jeu.GetPoints(joueur_id);

        // Assert
        Assert.Equal(expectedPoints, result);
    }

    [Theory]
    [InlineData(1, "15")]
    [InlineData(0, "15")]
    public void AddOnePoint(int joueur_id, string expectedPoints)
    {
        // Act
        jeu.AddPoint(joueur_id);
        string result = jeu.GetPoints(joueur_id);

        // Assert
        Assert.Equal(expectedPoints, result);
    }

    [Theory]
    [InlineData(1, 1)]
    [InlineData(0, 0)]
    public void AddFourPoints(int joueur_id, int expectedWinner)
    {
        // Act
        jeu.AddPoint(joueur_id);
        jeu.AddPoint(joueur_id);
        jeu.AddPoint(joueur_id);
        jeu.AddPoint(joueur_id);
        int winner = jeu.GetWinner();

        // Assert
        Assert.Equal(expectedWinner, winner);
    }

    [Theory]
    [InlineData(0, 1, 0)]
    [InlineData(1, 0, 1)]
    public void TestAdvantageCase(int joueur1_id, int joueur2_id, int expectedWinner)
    {
        // Act
        for(int i = 0; i < 3; i++)
        {
            jeu.AddPoint(joueur1_id);
            jeu.AddPoint(joueur2_id);
        }
        jeu.AddPoint(joueur1_id);
        jeu.AddPoint(joueur2_id);
        jeu.AddPoint(joueur1_id);
        jeu.AddPoint(joueur1_id);
        int winner = jeu.GetWinner();

        // Assert
        Assert.Equal(expectedWinner, winner);
    }

}