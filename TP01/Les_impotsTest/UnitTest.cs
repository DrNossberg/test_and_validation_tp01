using System;
using Xunit;
using Les_impots;

namespace Les_impotsTest;

public class UnitTest
{
    // private const string _messageExceptionParameterNull = "No args"; Erf ? How ?
    private const string _messageInvalideParameter = "Invalid parameters";

    Calculateur calc = new Calculateur();

    [Theory]
    [InlineData(0, _messageInvalideParameter )]
    [InlineData(-1, _messageInvalideParameter)]
    public void WrongArgs(int revenu_annuel, string msg)
    {
        ArgumentException ex = Assert.Throws<ArgumentException>(() => calc.getTaxesValue(revenu_annuel, 0));
        Assert.Equal(msg, ex.Message);
    }

    [Theory]
    [InlineData(9000, 0)]
    [InlineData(10778, 11)]
    [InlineData(27479, 30)]
    [InlineData(78571, 41)]
    [InlineData(168995, 45)]
    public void RightTaxeRate(int income, int rate)
    {
        Assert.Equal(calc.getTaxesRates(income), rate);
    }

    [Theory]
    [InlineData(4575, 0, 0)]
    [InlineData(10778, 11, 1185.58)]
    [InlineData(27479, 30, 8243.7)]
    [InlineData(78571, 41, 32214.11)]
    [InlineData(168995, 45, 76047.75)]
    public void RightTaxeValue(int income, int rate, float value)
    {
        Assert.Equal(calc.getTaxesValue(income, rate), value);
    }
}