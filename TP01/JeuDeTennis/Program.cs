﻿using System;
namespace JeuDeTennis;

public class Jeu
{

    int[] players_pts = { 0, 0 };
    string[] tennis_pts = { "0", "15", "30", "40", "A" };
    int winner = -1;

    public static void Main()
    {
        var jeu = new Jeu();
    }

    public string GetPoints(int player_id)
    {
        return tennis_pts[players_pts[player_id]];        
    }

    public void AddPoint(int player_id)
    {
        players_pts[player_id]++;
        if (players_pts[player_id] == 4)
        {
            if (players_pts[1 - player_id] == 4)
                players_pts[1 - player_id] = 3;
            else
                winner = player_id;
        }
    }

    public int GetWinner()
    {
        return winner;
    }
}