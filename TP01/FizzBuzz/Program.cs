﻿using System;
namespace FizzBuzz;

public class FizzBuzzGenerator
{
    static void Main(string[] args)
    {
        FizzBuzzGenerator generator = new FizzBuzzGenerator();

        Console.WriteLine(generator.Generer(int.Parse(args[0])));
    }

    public string Generer(int n) {
        string result = "";

        if (n <= 15 || n >= 150)
            return ("");
        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0)
                result += "Fizz";
            if (i % 5 == 0)
                result += "Barr";
            if (i % 3 != 0 && i % 5 != 0)
                result += Convert.ToString(i);
        }
        return (result);
    }
}

